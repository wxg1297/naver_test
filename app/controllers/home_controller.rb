require 'open-uri'
require 'json'
require "net/https"
require "uri"
require "faraday"

class HomeController < ApplicationController
  def index

  end

  def api

    @before = params[:translate]

    client_id = "UFWiyiAxPTPFBWbUHFWq"
    client_secret = "SL0RH222nq"

    url = "https://openapi.naver.com/v1/language/translate"

    uri = URI.parse(url)

    conn = Faraday.new(:url => url) do |c|
      c.use Faraday::Request::UrlEncoded  # encode request params as "www-form-urlencoded"
      c.use Faraday::Response::Logger     # log request & response to STDOUT
      c.use Faraday::Adapter::NetHttp     # perform requests with Net::HTTP
    end

    response = conn.post(url, {'source'=>'ko', 'target'=>'en', 'text'=>params[:translate]}, 'X-Naver-Client-Id' => client_id, 'X-Naver-Client-Secret' => client_secret)

    @after = JSON.parse(response.body)["message"]["result"]["translatedText"]

  end

  def before
    @before = params[:translate]
    uri = URI(URI.encode("https://openapi.naver.com/v1/search/movie.json?query=#{@before}"))

    req = Net::HTTP::Get.new(uri)
    req['X-Naver-Client-Id'] = "UFWiyiAxPTPFBWbUHFWq"
    req['X-Naver-Client-Secret'] = "SL0RH222nq"

    res = Net::HTTP.start(uri.hostname, uri.port, :use_ssl => uri.scheme == 'https') {|http|
      http.request(req)}

    @after = JSON.parse(res.body)["items"]
  end

  def speech
    @before = params[:text]

    client_id = "UFWiyiAxPTPFBWbUHFWq"
    client_secret = "SL0RH222nq"

    url = "https://openapi.naver.com/v1/voice/tts.bin"

    uri = URI.parse(url)

    conn = Faraday.new(:url => url) do |c|
      c.use Faraday::Request::UrlEncoded  # encode request params as "www-form-urlencoded"
      c.use Faraday::Response::Logger     # log request & response to STDOUT
      c.use Faraday::Adapter::NetHttp     # perform requests with Net::HTTP
    end

    response = conn.post(url, {'speaker'=> 'mijin', 'speed'=> 0, 'text'=> params[:text]}, 'X-Naver-Client-Id' => client_id, 'X-Naver-Client-Secret' => client_secret)

    after = response.body

    send_data after,  :filename => "sample.mp3"

  end
end
